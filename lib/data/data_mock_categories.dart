import 'package:flutter/material.dart';

import '../models/model_category.dart';

const dataMockCategories = [
  ModelCategory(
    id: 'c1',
    title: 'Italian',
    color: Colors.purple,
  ),
  ModelCategory(
    id: 'c2',
    title: 'Quick & Easy',
    color: Colors.red,
  ),
  ModelCategory(
    id: 'c3',
    title: 'Hamburgers',
    color: Colors.orange,
  ),
  ModelCategory(
    id: 'c4',
    title: 'German',
    color: Colors.amber,
  ),
  ModelCategory(
    id: 'c5',
    title: 'Light & Lovely',
    color: Colors.blue,
  ),
  ModelCategory(
    id: 'c6',
    title: 'Exotic',
    color: Colors.green,
  ),
  ModelCategory(
    id: 'c7',
    title: 'Breakfast',
    color: Colors.lightBlue,
  ),
  ModelCategory(
    id: 'c8',
    title: 'Asian',
    color: Colors.lightGreen,
  ),
  ModelCategory(
    id: 'c9',
    title: 'French',
    color: Colors.pink,
  ),
  ModelCategory(
    id: 'c10',
    title: 'Summer',
    color: Colors.teal,
  ),
];
