import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/model_meal.dart';

class NotifierFavs extends StateNotifier<List<ModelMeal>> {
  NotifierFavs() : super([]);

  bool toggleMealFavStatus(ModelMeal meal) {
    final bool isFav = state.contains(meal);

    state = isFav
        ? state.where((existingMeal) => existingMeal.id != meal.id).toList()
        : [...state, meal];

    return !isFav;
  }
}

final providerFavs = StateNotifierProvider<NotifierFavs, List<ModelMeal>>(
  (ref) => NotifierFavs(),
);
