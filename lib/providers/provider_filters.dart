import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/model_meal.dart';
import '../providers/provider_meals.dart';

enum FilterType { glutenFree, lactoseFree, vegetarian, vegan }

class NotifierFilters extends StateNotifier<Map<FilterType, bool>> {
  NotifierFilters()
      : super({
          FilterType.glutenFree: false,
          FilterType.lactoseFree: false,
          FilterType.vegetarian: false,
          FilterType.vegan: false,
        });

  void setFilters(Map<FilterType, bool> chosenFilters) {
    state = chosenFilters;
  }

  void setFilter(FilterType filter, bool isActive) {
    state = {
      ...state,
      filter: isActive,
    };
  }
}

final providerFilters =
    StateNotifierProvider<NotifierFilters, Map<FilterType, bool>>(
        (ref) => NotifierFilters());

final providerFilteredMeals = Provider<List<ModelMeal>>((ref) {
  final List<ModelMeal> meals = ref.watch(providerMeals);
  final Map<FilterType, bool> activeFilters = ref.watch(providerFilters);

  return meals.where((meal) {
    final bool notGlutenFree =
        activeFilters[FilterType.glutenFree]! && !meal.isGlutenFree;
    final bool notLactoseFree =
        activeFilters[FilterType.lactoseFree]! && !meal.isLactoseFree;
    final bool notVegetarian =
        activeFilters[FilterType.vegetarian]! && !meal.isVegetarian;
    final bool notVegan = activeFilters[FilterType.vegan]! && !meal.isVegan;

    if (notGlutenFree || notLactoseFree || notVegetarian || notVegan) {
      return false;
    }

    return true;
  }).toList();
});
