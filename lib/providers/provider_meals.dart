import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../data/data_mock_meals.dart';

final providerMeals = Provider((ref) => dataMockMeals);
