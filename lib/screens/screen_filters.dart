import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../providers/provider_filters.dart';
import '../widgets/filter.dart';

class ScreenFilters extends ConsumerWidget {
  const ScreenFilters({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Map<FilterType, bool> activeFilters = ref.watch(providerFilters);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Filters'),
      ),
      body: Column(
        children: <Widget>[
          Filter(
            'Gluten-free',
            description: 'Only include gluten-free meals.',
            stateSet: activeFilters[FilterType.glutenFree],
            onChangeFilterState: (isChecked) {
              ref
                  .read(providerFilters.notifier)
                  .setFilter(FilterType.glutenFree, isChecked);
            },
          ),
          Filter(
            'Lactose-free',
            description: 'Only include lactose-free meals.',
            stateSet: activeFilters[FilterType.lactoseFree],
            onChangeFilterState: (isChecked) {
              ref
                  .read(providerFilters.notifier)
                  .setFilter(FilterType.lactoseFree, isChecked);
            },
          ),
          Filter(
            'Vegetarian',
            description: 'Only for vegetarians.',
            stateSet: activeFilters[FilterType.vegetarian],
            onChangeFilterState: (isChecked) {
              ref
                  .read(providerFilters.notifier)
                  .setFilter(FilterType.vegetarian, isChecked);
            },
          ),
          Filter(
            'Vegan',
            description: 'Only for vegans.',
            stateSet: activeFilters[FilterType.vegan],
            onChangeFilterState: (isChecked) {
              ref
                  .read(providerFilters.notifier)
                  .setFilter(FilterType.vegan, isChecked);
            },
          ),
        ],
      ),
    );
  }
}
