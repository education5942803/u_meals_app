import 'package:flutter/material.dart';

import '../models/model_meal.dart';
import '../widgets/meal_item.dart';
import 'screen_meal_details.dart';

class ScreenMeals extends StatelessWidget {
  const ScreenMeals({
    super.key,
    this.title,
    required this.meals,
  });

  final String? title;
  final List<ModelMeal> meals;

  void _selectMeal(BuildContext context, ModelMeal meal) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => ScreenMealDetails(meal),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final bool isInsideScaffold = title == null;

    final Widget screenBody = meals.isEmpty
        ? Center(
            child: Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Oh no... there\' nothing here!',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          color: Theme.of(context).colorScheme.onBackground,
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  const SizedBox(height: 4.0),
                  Text(
                    'Hurry up — add smthng!',
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          color: Theme.of(context).colorScheme.onBackground,
                          fontSize: 24.0,
                        ),
                  ),
                ],
              ),
            ),
          )
        : ListView.builder(
            itemCount: meals.length,
            itemBuilder: (_, index) => MealItem(
              meal: meals[index],
              onSelectMeal: (meal) => _selectMeal(context, meal),
            ),
          );

    return isInsideScaffold
        ? screenBody
        : Scaffold(
            appBar: AppBar(
              title: Text(title!),
            ),
            body: screenBody,
          );
  }
}
