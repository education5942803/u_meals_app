import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/model_meal.dart';
import '../providers/provider_favs.dart';
import '../providers/provider_filters.dart';
import '../widgets/main_drawer.dart';
import 'screen_categories.dart';
import 'screen_filters.dart';
import 'screen_meals.dart';

const Map<FilterType, bool> kInitialFilters = {
  FilterType.glutenFree: false,
  FilterType.lactoseFree: false,
  FilterType.vegetarian: false,
  FilterType.vegan: false,
};

class ScreenTabs extends ConsumerStatefulWidget {
  const ScreenTabs({super.key});

  @override
  ConsumerState<ScreenTabs> createState() => _ScreenTabsState();
}

class _ScreenTabsState extends ConsumerState<ScreenTabs> {
  int _selectedPageIndex = 0;

  void _selectPage(int index) {
    setState(() => _selectedPageIndex = index);
  }

  void _setScreen(String identifier) async {
    Navigator.of(context).pop();

    if (identifier == 'filters') {
      await Navigator.of(context).push<Map<FilterType, bool>>(
        MaterialPageRoute(
          builder: (_) => const ScreenFilters(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<ModelMeal> availableMeals = ref.watch(providerFilteredMeals);

    Widget activePage;
    String activePageTitle;

    if (_selectedPageIndex == 1) {
      final favMeals = ref.watch(providerFavs);

      activePage = ScreenMeals(meals: favMeals);
      activePageTitle = 'Favs';
    } else {
      activePage = ScreenCategories(availableMeals: availableMeals);
      activePageTitle = 'Categories';
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(activePageTitle),
      ),
      drawer: MainDrawer(onSelectScreen: _setScreen),
      body: activePage,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        currentIndex: _selectedPageIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.set_meal),
            label: 'Categories',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Favs',
          ),
        ],
      ),
    );
  }
}
