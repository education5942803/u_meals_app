import 'package:flutter/material.dart';

class Filter extends StatelessWidget {
  const Filter(
    this.name, {
    super.key,
    required this.stateSet,
    required this.onChangeFilterState,
    this.description = '',
  });

  final String name;
  final String description;
  final stateSet;
  final void Function(bool isChecked) onChangeFilterState;

  @override
  Widget build(BuildContext context) {
    return SwitchListTile(
      value: stateSet,
      onChanged: onChangeFilterState,
      title: Text(
        name,
        style: Theme.of(context)
            .textTheme
            .titleLarge!
            .copyWith(color: Theme.of(context).colorScheme.onBackground),
      ),
      subtitle: Text(
        description,
        style: Theme.of(context)
            .textTheme
            .labelMedium!
            .copyWith(color: Theme.of(context).colorScheme.onBackground),
      ),
      activeColor: Theme.of(context).colorScheme.tertiary,
      contentPadding: const EdgeInsets.only(left: 34.0, right: 22.0),
    );
  }
}
